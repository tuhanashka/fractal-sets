# OpenGL Fractal Sets

Julia and Mandelbrot Fractal Sets written in c++ using OpenGL library

Julia Set:

![](OpenGL_Julia_Fractal_Set.png)

Mandelbrot Set:

![](OpenGL_Mandelbrot_Fractal_Set.png)